<?php

function load_verification_groups() {
    try {
        $verified = db_query("SELECT `data` FROM field_config where field_name='field_verify';");
        $verified = $verified->fetchField();
        $verified = unserialize($verified);
        $verified = $verified['settings']['allowed_values'];
    }
    catch (Exception $ex) {
        $verified = array("X19", "Проверенный");
        watchdog_exception(WATCHDOG_WARNING, $ex);
    }
    return $verified;
}

function multislice($str, $n) {
    if ($n < 1)
        $n = 1;
    $short = 32 * $n;
    return strlen($str) < $short ? $str
        : substr($str, 0, $short) . '...';
}

/**
 * Notice table (form)
 */
function notice_list($form, &$form_state) {
    $form['actions']['output'][] = array(
        '#theme' => 'menu_local_action',
        '#link' => menu_get_item('admin/content/notice/add')
    );

    $form['actions']['output'][] = array(
        '#type' => 'submit',
        '#value' => t('Удалить избранные')
    );

    $header = array(
        'en' => array('data'=>t('Английский'), 'field'=>'en'),
        'ru' => array('data'=>t('Русский'), 'field'=>'ru'),
        'uk' => array('data'=>t('Украинский'), 'field'=>'uk'),
        'verified' => array('data'=>t('Группа'), 'field'=>'role'),
        'region' => array('data'=>t('Регион'), 'field'=>'region'),
        'once' => array('data'=>t('Единоразово'), 'field'=>'once'),
    );

    $q = db_select('notice', 'm');
    $q->extend('PagerDefault');
    $q->fields('m', array());
    $notices = $q->execute();


    $notices_options = array();
    $verified = load_verification_groups();
    foreach($notices as $notice) {
        //@todo OPTIMIZE
        $regions = db_query('
          SELECT r.region FROM `notice_region` m
          JOIN `region` r ON m.tid = r.tid
          WHERE m.mid = :mid', array(':mid' => $notice->mid));

        $regions_list = array();
        while($region = $regions->fetchColumn(0))
            array_push($regions_list, "<div style=\"white-space: nowrap;\">$region</div>");
        $lines = count($regions_list);
        $notices_options[$notice->mid] = array(
            'en' => multislice($notice->en, $lines),
            'ru' => multislice($notice->ru, $lines),
            'uk' => multislice($notice->uk, $lines),
            'verified' => $verified[$notice->verified],
            'region' => $lines > 0 ? join("\n", $regions_list) : 'Все',
            'once' => $notice->once ? t('Да') : t('Нет'),
        );
    }

    $form['notices'] = array(
        '#type' => 'tableselect',
        '#header' => $header,
        '#options' => $notices_options,
        '#empty' => t('Уведомлений нету'),
    );

    $form['pager'] = array('#theme' => 'pager');
    return $form;
}


function notice_list_submit($form, &$form_state) {
    foreach($form_state['input']['notices'] as $i => $notice) {
        if ($notice) {
            db_query('DELETE m, md, r FROM `notice` m
            LEFT JOIN `noticed` md ON m.mid = md.mid
            LEFT JOIN `notice_region` r ON m.mid = r.mid
            WHERE m.mid = :mid',
                array(':mid' => (int)$notice));
        }
    }
}

/**
 * Create/edit notice form
 */
function notice_form($form, &$form_state) {
    notice_ui();

    function textarea($title) {
        return array(
            '#title' => t($title),
            '#type' => 'textarea',
            '#rows' => 2,
        );
    }

    $form['en'] = textarea('Английское');
    $form['ru'] = textarea('Русское');
    $form['uk'] = textarea('Украинское');

    // Проверенный
    $form['verified'] = array(
        '#title' => 'Группа',
        '#type' => 'select',
        '#options' => load_verification_groups()
    );

    // Список регионов
    $regions = db_query('
      SELECT * FROM `region`
      ORDER BY country desc, region
    ');
    $regions_options = array();
    foreach($regions as $region)
        $regions_options[$region->tid] = "$region->region, $region->country";

    $form['region'] = array(
        '#title' => 'Регион',
        '#type' => 'select',
        '#default_value' => 16,
        '#options' => $regions_options
    );

    $form['once'] = array(
        '#title' => t('Единоразово'),
        '#type' => 'checkbox',
        '#default_value' => 1
    );

    $form['submit_button'] = array(
        '#value' => t('Уведомить'),
        '#type' => 'submit'
    );
    return $form;
}

function notice_form_validate($form, &$form_state)
{
    $_ = $form_state['input'];
    if ( ! ($_['en'] or $_['ru'] or $_['uk'])) {
        form_set_error('', t('Текст увидомления должно быть заполнено хотя бы для одного языка'));
    }
}

function by_keys($in, $keys) {
    $out = array();
    foreach($keys as $key)
        $out[$key] = $in[$key];
    return $out;
}

function notice_form_submit($form, &$form_state) {
    $_ = $form_state['input'];

    $regions = isset($_['region']) ?
        (is_array($_['region']) ? $_['region'] : array($_['region']))
        : array();
    $transaction = db_transaction('notice_insert');
    try {
        $notice = by_keys($_, array('en', 'ru', 'uk', 'verified', 'once'));
        $notice['once'] = $notice['once'] ? 1 : 0;
        $mid = db_insert('notice')
            ->fields($notice)
            ->execute();
        #DOTO: OPTIMIZE
        foreach($regions as $region) {
            $q = db_insert('notice_region');
            $q->fields(array('mid' => $mid, 'tid' => $region));
            $q->execute();
        }
    }
    catch (Exception $e) {
        $transaction->rollback();
        watchdog_exception(WATCHDOG_ERROR, $e);
        drupal_set_message(t('Ошыбка выполнения транзакции ') . $e->getMessage());
        return;
    }
    drupal_goto('admin/content/notice');
}