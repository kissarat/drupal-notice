var KeyCodes = {
    Escape: 27
};
Object.freeze(KeyCodes);

addEventListener('DOMContentLoaded', function() {
    var $notices = jQuery('.notice');
    if ($notices.length > 0)
            notice_message($notices);
    var $region = jQuery('#edit-region');
    if ($region.length > 0)
        notice_admin_form($region);
});

function notice_message($notices) {
    var $ = jQuery;

    $notices.remove();
    var notices = [];
    $notices.find('li').each(function(_, $n) {
        notices.push($n);
    });
    if (0 == notices.length)
        notices.push($notices.html());
    $notices = $('<div id="notices" class="notice"></div>');
    var $list = $('<ul></ul>');

    var $back = $('<div id="black"></div>');
    var hide_back = animation_hide.bind($back[0]);
    function hide() {
        window.removeEventListener('click', hide);
        animation_hide.call(this, function() {
            if (0 == $notices.find('li').length)
                hide_back();
        });
    }

    for(var i in notices)
        $('<li></li>')
            .html(notices[i])
            .click(hide)
            .appendTo($list)

    $notices.append($list);
    $back
        .append($notices)
        .prependTo('body');


    function escape(e) {
        if (KeyCodes.Escape == e.keyCode) {
            window.removeEventListener('keyup', escape);
            var messages = $notices.find('li').toArray();
            cascadeTimout(240, function() {
                var message = messages.shift();
                if (message)
                    hide.call(message);
                else
                    hide_back();
                return !message;
            })
        }
    }

    window.addEventListener('keyup', escape);
}

function notice_admin_form(region) {
    var $ = jQuery;
    var regions = $('<div></div>');
    regions.append(region);
    $('[for="edit-region"]').after(regions);
    var region_container = $('<div></div>')
        .appendTo(regions);
    //.append(region);
    var region_order = [];
    var region_list = {};
    var tid;
    region.find('option').each(function(_, option) {
        var name = option.innerHTML;
        tid = parseInt(option.getAttribute('value'));
        region_order.push(tid);
        region_list[tid] = name;
    });
    region.remove();
    region = null;
    $('<input type="button" class="form-submit"/>')
        .val('Добавить')
        .appendTo(regions.parent())
        .click(function() {
            var remove = $('<input type="button" class="form-submit"/>')
                .val('Удалить');
            var selected;
            var region_name;
            if (region) {
                remove.click(function () {
                    var parent = $(this).parent();
                    parent.remove();
                });
                tid = region.val();
                tid = parseInt(tid);
                delete region_list[tid];
                selected = region_order.indexOf(tid) + 1;
                if (region_list[region_order[selected]])
                    selected = region_order[selected];
            }
            region = $('<select name="region[]" class="form-select"></select>');
            for (var j in region_order) {
                tid = region_order[j];
                region_name = region_list[tid];
                if (region_name)
                    $('<option></option>')
                        .val(tid)
                        .html(region_name)
                        .appendTo(region);
            }
            if (selected)
                region.val(selected);
            region_container = $('<div class="region"></div>')
                .appendTo(regions)
                .append(region)
                .append(remove);
        })
}


function cascadeTimout(timeout, call) {
    if (!call())
        setTimeout(cascadeTimout.bind(this, timeout, call), timeout);
}

function animation_hide(call) {
    var $ = jQuery;
    var block = this;
    var $block = $(this);
    var style = getComputedStyle(block);
    var height = parseInt(style.height);
    var opacity = 1;
    var last = Date.now();
    //var start = last;
    block.style.height = height + 'px';
    block.style.minHeight = '0';

    function finish() {
        $block.remove();
        if (call)
            call();
    }

    function delta(coefficient) {
        var now = Date.now();
        var d = now - last;
        //var t = now - start;
        last = now;
        return d / coefficient;
        //return [d / coefficient, t];
    }

    function height_frame() {
        height -= delta(8);
        block.style.height = height + 'px';
        if (height > 0)
            requestAnimationFrame(height_frame);
        else
            finish();
    }

    function opacity_frame(resize) {
        opacity -= 300/delta(300);
        //opacity = Math.pow(opacity, 4);
        block.style.opacity = opacity.toString();
        if (opacity > 0.01)
            requestAnimationFrame(opacity_frame);
        else
        if (resize)
            requestAnimationFrame(height_frame);
        else
            finish();
    }

    requestAnimationFrame(opacity_frame.bind(block, true));
}